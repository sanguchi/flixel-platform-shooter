package;

import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.display.FlxZoomCamera;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.ui.FlxVirtualPad;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;
import flixel.util.FlxStringUtil;
import openfl.Assets;

class PlayState extends FlxState
{
	
	public var worldAxis: FlxPoint;
	
	/**
	 * The FlxTilemap we're using
	 */ 
	var _collisionMap:FlxTilemap;

	/**
	 * Box to show the user where they're placing stuff
	 */ 
	var _highlightBox:FlxSprite;
	
	/**
	 * Player modified from "Mode" demo
	 */ 
	var _player:Player;
	
	override public function create():Void
	{
		// FlxG.mouse.visible = false;
		worldAxis = new FlxPoint();
		
		// Creates a new tilemap with no arguments
		_collisionMap = new FlxTilemap();
		
		
		loadMap();
		add(_collisionMap);
		
		_player = new Player(128, 204);
		add(_player);
		FlxG.camera.follow(_player, TOPDOWN, 1);
	}
	
	override public function update(elapsed:Float):Void
	{
		// Tilemaps can be collided just like any other FlxObject, and flixel
		// automatically collides each individual tile with the object.
		FlxG.collide(_player, _collisionMap);
		// TODO: OUT OF BOUNDS
		// FlxG.watch.addQuick("Out", FlxG.overlap(_player, ));
		boundsCheck();
		// _highlightBox.x = Math.floor(FlxG.mouse.x / TILE_WIDTH) * TILE_WIDTH;
		// _highlightBox.y = Math.floor(FlxG.mouse.y / TILE_HEIGHT) * TILE_HEIGHT;
		/*
		if (FlxG.mouse.pressed)
		{
			// FlxTilemaps can be manually edited at runtime as well.
			// Setting a tile to 0 removes it, and setting it to anything else will place a tile.
			// If auto map is on, the map will automatically update all surrounding tiles.
			_collisionMap.setTile(Std.int(FlxG.mouse.x / TILE_WIDTH), Std.int(FlxG.mouse.y / TILE_HEIGHT), FlxG.keys.pressed.SHIFT ? 0 : 1);
		}
		*/
		super.update(elapsed);
	}
	function loadMap(): Void
	{
		trace(worldAxis);
		// Initializes the map using the generated string, the tile images, and the tile size
		_collisionMap.loadMapFromCSV("assets/levels/room" + Std.string(worldAxis.x) + "-" + Std.string(worldAxis.y) + ".csv", "assets/empty_tiles.png", TILE_WIDTH, TILE_HEIGHT, OFF);
		// _collisionMap.loadMapFromCSV("assets/room-0.csv", "assets/empty_tiles.png", TILE_WIDTH, TILE_HEIGHT, OFF);
		_collisionMap.follow();
		_collisionMap.setTileProperties(0, FlxObject.NONE);
		_collisionMap.setTileProperties(1, FlxObject.ANY);
		
	}
	function boundsCheck(): Void
	{
		if ((_player.x + _player.frameWidth / 2) <= 0)
		{
			_player.x = _collisionMap.width;
			worldAxis.x -= 1;
			loadMap();
		}
		else if (_player.x >= _collisionMap.width)
		{
			_player.x = 0;
			worldAxis.x += 1;
			loadMap();
		}
		
		if ((_player.y + _player.frameHeight / 2) <= 0) 
		{
			_player.y = _collisionMap.height;
			worldAxis.y -= 1;
			loadMap();
		}
		else if (_player.y >= _collisionMap.height)
		{
			_player.y = 0;
			worldAxis.y += 1;
			loadMap();
		}
	}
	/*
	function onAlt():Void
	{
		switch (_collisionMap.auto)
		{
			case AUTO:
				_collisionMap.loadMapFromCSV(FlxStringUtil.arrayToCSV(_collisionMap.getData(true), _collisionMap.widthInTiles),
					"assets/alt_tiles.png", TILE_WIDTH, TILE_HEIGHT, ALT);
				_autoAltButton.label.text = "ALT";
			
			case ALT:
				_collisionMap.loadMapFromCSV(FlxStringUtil.arrayToCSV(_collisionMap.getData(true), _collisionMap.widthInTiles),
					"assets/empty_tiles.png", TILE_WIDTH, TILE_HEIGHT, OFF);
				_autoAltButton.label.text = "OFF";
			
			case OFF:
				_collisionMap.loadMapFromCSV(FlxStringUtil.arrayToCSV(_collisionMap.getData(true), _collisionMap.widthInTiles),
					"assets/auto_tiles.png", TILE_WIDTH, TILE_HEIGHT, AUTO);
				_autoAltButton.label.text = "AUTO";
		}
	}
	
	function onReset():Void
	{
		switch (_collisionMap.auto)
		{
			case AUTO:
				_collisionMap.loadMapFromCSV("assets/default_auto.txt", "assets/auto_tiles.png", TILE_WIDTH, TILE_HEIGHT, AUTO);
				_player.setPosition(64, 220);
				
			case ALT:
				_collisionMap.loadMapFromCSV("assets/default_alt.txt", "assets/alt_tiles.png", TILE_WIDTH, TILE_HEIGHT, ALT);
				_player.setPosition(64, 128);
				
			case OFF:
				_collisionMap.loadMapFromCSV("assets/default_empty.txt", "assets/empty_tiles.png", TILE_WIDTH, TILE_HEIGHT, OFF);
				_player.setPosition(64, 64);
		}
	}
	*/
}