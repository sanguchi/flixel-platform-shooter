package;

import flixel.FlxBasic;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxSubState;
import flixel.addons.display.FlxZoomCamera;
import flixel.effects.particles.FlxEmitter;
import flixel.group.FlxGroup;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.tweens.FlxTween;
import flixel.ui.FlxButton;
import flixel.ui.FlxVirtualPad;
import flixel.util.FlxAxes;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;
import flixel.util.FlxStringUtil;
import openfl.Assets;
using Lambda;
class RoomState extends FlxState
{
	/**
	 * Some static constants for the size of the tilemap tiles
	 */
	static inline var TILE_WIDTH:Int = 16;
	static inline var TILE_HEIGHT:Int = 16;
	
	// Current position in the world map.
	public var worldAxis: FlxPoint;
	
	
	// should i keep the player here or add it to the maps as json data?
	var _player: Player;
	
	// group to hold bullets.
	public var _bullets: FlxTypedGroup<Bullet>;
	
	// group to hold level items.
	public var _items: FlxTypedGroup<FlxSprite>;
	
	/**
	 * The FlxTilemap we're using
	 */ 
	var _collisionMap:FlxTilemap;
	
	public var _winArea: WinArea;
	
	override public function create():Void
	{
		// initialize groups and add them to the state.
		_bullets = new FlxTypedGroup<Bullet>(20);
		_items = new FlxTypedGroup<FlxSprite>();
		
		add(_bullets);
		add(_items);
		
		// Don't need the mouse in this game
		#if FLX_MOUSE
		FlxG.mouse.visible = false;
		#end
		
		// Start the world axis at 0,0 (top-left corner of the world)
		worldAxis = new FlxPoint();
		
		// Get Tilemap from loader.
		_collisionMap = RoomLoader.loadFromAxis(worldAxis, new FlxTilemap());
		
		// set up tilemap and state to keep the camera focused on the map, so we don't see anything outside it.
		_collisionMap.follow();
		
		// Fill the state with the object layer made in Tiled.
		RoomLoader.populateObjects(this, worldAxis);
		
		// add the level to the state.
		add(_collisionMap);
		
		// Get player coords if it is the first level/room.
		var playerCoords = RoomLoader.getPlayerCoords(worldAxis);
		// If coords are not null, this means we are in the first level.
		if (playerCoords != null) {
			_player = new Player(playerCoords.x, playerCoords.y, this._bullets);
		}
		
		// add player to the level on top of everything.
		add(_player);
		
		// tell game camera to follow the player.
		FlxG.camera.follow(_player, TOPDOWN, 1);
		
		#if debug
		trace("RoomState created");
		FlxG.console.registerClass(Bullet);
		FlxG.console.registerClass(Box);
		FlxG.console.registerClass(FlxEmitter);
		/*
		var w: WinArea = new WinArea(FlxG.width / 2, FlxG.height / 2);
		// add(w._emitter);
		
		// w._emitter.lifespan = 
		*/
		// FlxG.console.registerObject("emitter", w._emitter);
		#end
		
	}
	
	override public function update(elapsed:Float):Void
	{
		// Tilemaps can be collided just like any other FlxObject, and flixel
		// automatically collides each individual tile with the object.
		FlxG.collide(_player, _collisionMap);
		FlxG.collide(_bullets, _collisionMap);
		FlxG.collide(_items, _collisionMap);
		
		// Collision within object in the game.
		// bump player against items/boxes
		FlxG.collide(_player, _items);
		// don't let boxes get out of the room.
		_items.forEach(function(o: FlxSprite){ FlxSpriteUtil.bound(o, 16, _collisionMap.width -16, 16, _collisionMap.height -16); }); 
		// bounce boxes when they get collided by a bullet.
		FlxG.collide(_bullets, _items, bumpBox);
		FlxG.overlap(_player, _items, function(player, box) { _player.velocity.y = 0; });

		boundsCheck();
		winCondition();
		super.update(elapsed);		
	}
	
	
	function loadMap(): Void
	{
		trace(worldAxis); 
		// Initializes the map using the generated string, the tile images, and the tile size
		// _collisionMap.loadMapFromCSV("assets/levels/room" + Std.string(worldAxis.x) + "-" + Std.string(worldAxis.y) + ".csv", "assets/empty_tiles.png", TILE_WIDTH, TILE_HEIGHT, OFF);
		// _collisionMap.loadMapFromCSV("assets/room-0.csv", "assets/empty_tiles.png", TILE_WIDTH, TILE_HEIGHT, OFF);
		_collisionMap.follow();
		_collisionMap.setTileProperties(0, FlxObject.NONE);
		_collisionMap.setTileProperties(1, FlxObject.ANY);
		
	}
	
	function bumpBox(bullet: Dynamic, box: Dynamic) {
		var box: Box = cast(box, Box);
		var bullet: Bullet = cast(bullet, Bullet);
		box.y -= 2;
		box.velocity.y -= 5;
		// trace('BULLET FACING: ${bullet.facing}');
		switch(bullet.facing) {
			case FlxObject.RIGHT:
				box.velocity.x -= 400;
			case FlxObject.LEFT:
				box.velocity.x += 400;
		}
	}
	
	function boundsCheck(): Void
	{
		if ((_player.x + _player.frameWidth / 2) <= 0)
		{
			_player.x = _collisionMap.width;
			worldAxis.x -= 1;
			// loadMap();
			_collisionMap = RoomLoader.loadFromAxis(worldAxis, _collisionMap);
			_bullets.clear();
			// _collisionMap.follow();
		}
		else if (_player.x >= _collisionMap.width)
		{
			_player.x = 0;
			worldAxis.x += 1;
			// loadMap();
			_collisionMap = RoomLoader.loadFromAxis(worldAxis, _collisionMap);
			// _collisionMap.follow();
			_bullets.clear();
		}
		
		if ((_player.y + _player.frameHeight / 2) <= 0) 
		{
			_player.y = _collisionMap.height;
			worldAxis.y -= 1;
			// loadMap();
			_collisionMap = RoomLoader.loadFromAxis(worldAxis, _collisionMap);
			// _collisionMap.follow();
			_bullets.clear();
		}
		else if (_player.y >= _collisionMap.height)
		{
			_player.y = 0;
			worldAxis.y += 1;
			// loadMap();
			_collisionMap = RoomLoader.loadFromAxis(worldAxis, _collisionMap);
			// _collisionMap.follow();
			_bullets.clear();
		}
	}
	
	function winCondition(): Void {
		if (worldAxis.y == 0 && worldAxis.x == 0) {
			this._winArea._emitter.visible = true;
			if (FlxG.overlap(this._winArea, this._items)) {
				// trace("WON");
				_items.forEachAlive(function(box: FlxSprite){
					if( box.alpha == 1) {
						FlxSpriteUtil.fadeOut(box, 1, function(t: FlxTween) {
							this.openSubState(new WinState());
						});
					}
				});
			}
		} else {
			// this._winArea.visible = false;
			this._winArea._emitter.visible = false;
		}
		
	}
	
}

