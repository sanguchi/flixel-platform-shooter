package;

import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.graphics.FlxGraphic;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;
import flixel.graphics.frames.FlxTileFrames;
import flixel.group.FlxGroup;
#if FLX_GAMEPAD
import flixel.input.gamepad.FlxGamepad;
#end
import flixel.math.FlxPoint;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxSpriteUtil;
import flixel.FlxG;
import flixel.util.FlxTimer;
import openfl.Assets;

#if android
import flixel.ui.FlxVirtualPad;
#end

/**
 * ...
 * @author Peka
 */
class Player extends FlxSprite 
{
	#if android
	public var virtualGamepad(default, null): FlxVirtualPad;
	public var restartButton: FlxButton;
	#end	
	
	
	
	static var FIRE_RATE:Float = 1 / 10; // 10 shots per second
	var _shootTimer = new FlxTimer();
	// public var shooting(default, null): Bool;
	var _bullets: FlxTypedGroup<Bullet>;
	public var restartText: FlxText;
	
	
	public function new(?X:Float=0, ?Y:Float=0, bullets: FlxTypedGroup<Bullet>) 
	{
		super(X, Y);
		this.loadGraphic("assets/graphics/spaceman.png", true, 16);
		
		// Bounding box tweaks
		this.setSize(14, 14);
		this.offset.set(1, 1);
		
		// Basic player physics
		this.drag.x = 640;
		this.acceleration.y = 420;
		this.maxVelocity.set(120, 200);
		
		// Animations
		this.animation.add("idle", [0]);
		this.animation.add("run", [1, 2, 3, 0], 12);
		this.animation.add("jump", [4]);
		
		
		
		#if android
		// Android on-screen virtual Gamepad
		this.virtualGamepad = new FlxVirtualPad(FlxDPadMode.LEFT_RIGHT, FlxActionMode.A_B);
		this.virtualGamepad.alpha = 0.5;
		FlxG.state.add(this.virtualGamepad);		
		// Restart button.
		restartButton = new FlxButton(20, 20, "Restart", function() { FlxG.resetState(); });
		restartButton.label.color = 0xffffff;
		restartButton.loadGraphic("assets/graphics/restartButton.png", true, 80, 20);
		FlxG.state.add(restartButton);
		#else
		restartText = new FlxText(16, 16, 0, "Press R to restart");
		FlxG.state.add(restartText);
		#end
		
		
		
		_bullets = bullets;
		
		#if debug
		// FlxG.debugger.track(this._bullets);
		#end
	}
	public function jump(): Void
	{
		if (this.velocity.y == 0)
		{
			this.y -= 1;
			this.velocity.y = -200;
		}
	}
	override public function update(elapsed:Float):Void 
	{
		// FlxSpriteUtil.screenWrap(this);
		
		// MOVEMENT
		this.acceleration.x = 0;
		
		#if FLX_KEYBOARD
		updateKeyboard();
		#end
		
		#if FLX_GAMEPAD
		updateJoystick();
		#end
		
		#if android
		updateVirtualPad();
		#end
		
		updateAnimations();
		super.update(elapsed);
	}
	function move(dir: Int): Void {
		switch(dir){
			case FlxObject.LEFT: {
				this.flipX = true;
				this.acceleration.x -= this.drag.x;
				
			}
			case FlxObject.RIGHT: {
				this.flipX = false;
				this.acceleration.x += this.drag.x;
			}
		}
		this.facing = dir;
	}
	function updateKeyboard(): Void{
		#if FLX_KEYBOARD
		if (FlxG.keys.anyPressed([LEFT, A]))
			move(FlxObject.LEFT);
		else if (FlxG.keys.anyPressed([RIGHT, D]))
			move(FlxObject.RIGHT);
			
		if (FlxG.keys.anyJustPressed([UP, W]))
			jump();
			
		if (FlxG.keys.justPressed.R) 
			FlxG.resetState();
			
		if (FlxG.keys.justPressed.SPACE)
			shoot();
		#end
	}
	
	function updateJoystick(): Void {
		#if FLX_GAMEPAD
		var gamepad: FlxGamepad = FlxG.gamepads.lastActive;
		if (gamepad == null) {
			restartText.text = "Press R to restart";
			return;
		}
		
		restartText.text = "Press Start to restart";
		if (gamepad.analog.value.LEFT_STICK_X < 0 || gamepad.pressed.DPAD_LEFT)
			move(FlxObject.LEFT);
		else if (gamepad.analog.value.LEFT_STICK_X > 0 || gamepad.pressed.DPAD_RIGHT)
			move(FlxObject.RIGHT);
			
		if (gamepad.justPressed.A)
			jump();
			
		if (gamepad.justPressed.B)
			shoot();
			
		if (gamepad.pressed.START)
			FlxG.resetState();
		#end
	}
	function updateVirtualPad(): Void {
		#if android
		if (virtualGamepad.buttonLeft.pressed)
			move(FlxObject.LEFT);
		else if (virtualGamepad.buttonRight.pressed)
			move(FlxObject.RIGHT);
		if (virtualGamepad.buttonA.justPressed)
			jump();
		if (virtualGamepad.buttonB.justPressed)
			shoot();
		#end
	}

	function shoot(): Void {
		if (_shootTimer.active)
			return;
		_shootTimer.start(FIRE_RATE);
		getMidpoint(_point);
		_bullets.recycle(Bullet.new).shoot(_point, this.facing);
			
	}

	
	function updateAnimations(): Void {
		// ANIMATION
		if (this.velocity.y != 0)
		{
			this.animation.play("jump");
		}
		else if (this.velocity.x == 0)
		{
			this.animation.play("idle");
		}
		else
		{
			this.animation.play("run");
		}
	}
}