package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;

/**
 * ...
 * @author Peka
 */
class Box extends FlxSprite 
{

	public function new(?X:Float=0, ?Y:Float=0, ?SimpleGraphic:FlxGraphicAsset) 
	{
		super(X, Y, SimpleGraphic);
		this.loadGraphic("assets/graphics/box.png");
		this.acceleration.y = 210;
		this.drag.x = 640;
	}
	
}