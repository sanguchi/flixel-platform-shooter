package;
import flixel.FlxBasic;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.math.FlxPoint;
import flixel.tile.FlxBaseTilemap.FlxTilemapAutoTiling;
import flixel.tile.FlxTile;
import flixel.tile.FlxTilemap;
import haxe.Json;
import openfl.Assets;
import flixel.FlxG;
using Lambda;

/**
 * ...
 * @author Peka
 */
class RoomLoader 
{

	/**
	 * Some static constants for the size of the tilemap tiles
	 */
	// static inline var TILE_WIDTH:Int = 16;
	// static inline var TILE_HEIGHT:Int = 16;
	
	public function new() {}
	
	public static function loadFromAxis(axis: FlxPoint, ?tilemap: FlxTilemap): FlxTilemap
	{
		var tilesFile: String = "assets/graphics/game_tiles.png";
		var project: ProjectData = getProjectData(axis);
		// trace(project);
		
		if (tilemap == null) {
			tilemap = new FlxTilemap();
		}
		
		var layerData: TileLayer = RoomLoader.getTileLayer(project);
		trace('Loading map on axis $axis');
		layerData.data = layerData.data.map(function(i:Int){return i - 1; });
		layerData.data.mapi(function(index:Int, item:Int){
			if(index % layerData.width == 0) {
				// trace(layerData.data.copy().slice(index - layerData.width, index));
			}
		});
		
		
		
		// trace(layerData);
		tilemap.loadMapFromArray(layerData.data, project.width, project.height, tilesFile, project.tilewidth, project.tileheight, FlxTilemapAutoTiling.OFF);
		// tilemap.loadMapFromCSV(mapName, tilesFile, TILE_WIDTH, TILE_HEIGHT, OFF);
		tilemap.setTileProperties(0, FlxObject.NONE);
		tilemap.setTileProperties(1, FlxObject.ANY);
		return tilemap;
	}
	public static function getPlayerCoords(axis: FlxPoint): FlxPoint
	{
		var project: ProjectData = getProjectData(axis);
		var objects: ObjectGroup = getObjectGroup(project);
		var player: ObjectData = objects.objects.find(function (o: ObjectData) { return o.type == "Player"; });
		if (player != null) {
			return new FlxPoint(player.x, player.y);
		}
		return null;
	}
	public static function populateObjects(state: RoomState, axis: FlxPoint): FlxState
	{
		var project: ProjectData = getProjectData(axis);
		var objects: ObjectGroup = getObjectGroup(project);
		for (object in objects.objects) {
			switch (object.type) {
				case "Player": {
					/*
					var player: Player = cast(state.members.find(function (f: FlxBasic) { return Std.is(f, Player); }), Player);
					if (player != null) {
						player.x = object.x;
						player.y = object.y;
					} else {
						state.add(new Player(object.x, object.y));
					}
					*/
				}
				/*
				case "Bullet": {
					var bullet: Bullet = new Bullet(object.x, object.y);
					state._items.add(bullet);
				}
				*/
				case "Box": {
					var box: Box = new Box(object.x, object.y);
					state._items.add(box);
				}
				
				case "WinArea": {
					var winArea: WinArea = new WinArea(object.x, object.y, object.width, object.height);
					// winArea.width = object.width;
					// winArea.height = object.height;
					state._winArea = winArea;
					state.add(winArea);
					state.add(winArea._emitter);
					// winArea.emit();
				}
			}
		}
		return state;
	}
	
	private static function getProjectData(axis: FlxPoint): ProjectData
	{
		var mapName: String = 'assets/rooms/room${axis.x}-${axis.y}.json';
		// trace('Loading file $mapName');
		var project: ProjectData = Json.parse(Assets.getText(mapName));
		return project;
	}
	
	private static function getTileLayer(project: ProjectData): TileLayer
	{
		for (value in project.layers)
		{
			if (value.type == "tilelayer") {
				var r: TileLayer = value;
				return r;
			}
		}
		
		return null;
	}
	
	private static function getObjectGroup(project: ProjectData): ObjectGroup
	{
		for (value in project.layers)
		{
			if (value.type == "objectgroup") {
				var r: ObjectGroup = value;
				return r;
			}
		}
		
		return null;
	}
}

typedef ObjectData = {
	var name: String;
	var x: Int;
	var y: Int;
	var type: String;
	var visible: Bool;
	var height: Int;
	var width : Int;
	var point: Bool;
}

typedef ObjectGroup = {
	var name: String;
	var objects: Array<ObjectData>;
	var type: String;
	var visible: Bool;
}

typedef TileLayer = {
	var data: Array<Int>;
	var height: Int;
	var width: Int;
	var id: Int;
	var name: String;
}

typedef ProjectData = {
	var height: Int;
	var width: Int;
	var layers: Array<Dynamic>;
	var tileheight: Int;
	var tilewidth: Int;
}