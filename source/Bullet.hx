package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.FlxObject;
/**
 * ...
 * @author Peka
 */
class Bullet extends FlxSprite
{
	var _speed:Float;
	
	public function new()
	{
		super();
		
		// loadGraphic(AssetPaths.bullet__png, true);
		makeGraphic(4, 2);
		width = 6;
		height = 2;
		offset.set(1, 1);
		_speed = 360;
	}
	
	override public function update(elapsed:Float):Void
	{
		if (!alive)
		{
			exists = false;
		}
		else if (touching != 0)
		{
			kill();
		}
		if ( !isOnScreen()) {
			kill();
		}
		super.update(elapsed);
	}
	
	override public function kill():Void
	{
		if (!alive)
			return;
		velocity.set(0, 0);
		alive = false;
		solid = false;
	}
	
	public function shoot(Location:FlxPoint, Aim:Int):Void
	{
		super.reset(Location.x - width / 2, (Location.y - height / 2) + 4);
		this.facing = Aim;
		solid = true;
		switch (Aim)
		{
			case FlxObject.LEFT:
				velocity.x = - _speed;
			case FlxObject.RIGHT:
				x += 6;
				velocity.x = _speed;
		}
	}
}