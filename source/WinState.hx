package;

import flixel.FlxSubState;
import flixel.util.FlxColor;
import flixel.text.FlxText;
import flixel.util.FlxAxes;
import flixel.FlxG;

#if android
import flixel.ui.FlxButton;
#end


/**
 * ...
 * @author Peka
 */
class WinState extends FlxSubState 
{
	#if android
	var restartButton: FlxButton;
	#else
	var restartText: FlxText;
	#end
	
	public function new(BGColor:FlxColor=FlxColor.TRANSPARENT) 
	{
		super(0x99808080);
		// var winState: FlxSubState = new FlxSubState(0x99808080);		
		var winText: FlxText = new FlxText(16, 16, 0, "You won!", 16);
		winText.screenCenter(FlxAxes.XY);
		winText.y -= 32;
		this.add(winText);
		
		#if android
		restartButton = new FlxButton(20, 20, "Restart", function() { FlxG.resetState(); });
		restartButton.label.color = 0xffffff;
		restartButton.loadGraphic("assets/graphics/restartButton.png", true, 80, 20);
		restartButton.screenCenter(FlxAxes.XY);		
		this.add(restartButton);
		
		#else
		restartText = new FlxText(16, 16, 0, "Press R to restart");
		restartText.screenCenter(FlxAxes.XY);
		this.add(restartText);
		#end
	}
	
	override public function update(elapsed:Float):Void 
	{
		#if !android
		if (FlxG.keys.justPressed.R) { FlxG.resetState(); }
		#end
		super.update(elapsed);
	}
}