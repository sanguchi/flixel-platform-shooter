package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.effects.particles.FlxEmitter;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.util.FlxColor;

/**
 * ...
 * @author Peka
 */
class WinArea extends FlxSprite 
{
	public var _emitter: FlxEmitter;
	public function new(X: Float, Y: Float, Width: Int, Height: Int) 
	{
		super(X, Y);
		this.visible = false;
		this.width = Width;
		this.height = Height;
		_emitter = new FlxEmitter(this.x, this.y + this.height, 20);
		_emitter.width = this.width;
		_emitter.height = 1;
		_emitter.makeParticles(2, 2, FlxColor.WHITE, 20);
		_emitter.launchMode = FlxEmitterMode.SQUARE;
		_emitter.velocity.set(0, -10, 0, -20, 0, -10, 0, -20);
		_emitter.start(false, 0.1);
	}
	
	override public function update(elapsed:Float):Void 
	{
		if( visible)
			super.update(elapsed);
	}
}