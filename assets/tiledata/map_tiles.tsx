<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="map_tiles" tilewidth="16" tileheight="16" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1">
  <image width="16" height="16" source="../graphics/background_wall.png"/>
 </tile>
 <tile id="2">
  <image width="16" height="16" source="../graphics/foreground_wall.png"/>
 </tile>
</tileset>
